(function() {
if("undefined" === typeof(__ZSAD)){
    var __ZSAD = {}
    window.__ZSAD = __ZSAD;
    __ZSAD.bootURL = "//www.zoho.com/sites/js/zsad.js"
    __ZSAD.docScripts = [];
    __ZSAD.adSpaces = [];
    __ZSAD.firstRun = true;
    __ZSAD.getID = function() {
        return Math.floor(Math.random()*10000000000000)
    }
    __ZSAD.findScripts = function(){
        var scripts = document.getElementsByTagName("script");
        var len = scripts.length;
        for(var i=0;i<len;i++) {
            var script = scripts[i];
            if(script.getAttribute("data-zsada-id")==null && script.src.indexOf(__ZSAD.bootURL)!=-1) {
                script.setAttribute("data-zsada-id",__ZSAD.getID());
                __ZSAD.docScripts.push(script);
            }
        }
    }
    __ZSAD.processScripts = function() {
        var len = __ZSAD.docScripts.length;
        for(var i=0;i<len;i++) {
            var script = __ZSAD.docScripts[i];
            
            var scriptID = script.getAttribute("data-zsada-id");
            var addiv = document.createElement("div");
            addiv.id="zsada-"+scriptID;
            addiv.innerHTML = "<div style=\"height: 25px ! important; padding: 20px 0px; font-size: 13px; font-family: Arial, Helvetica, sans-serif\"><p align=\"center\"> <a href=\"http://www.zohosites.com/?utm_source=footer-banner&amp;utm_medium=ad&amp;utm_campaign=footer-banner\"   rel=\"nofollow\" style=\"cursor: pointer;\"> <span style=\"vertical-align: top;\">Create a Free Website</span><img height=\"16px\" src=\"//www.zoho.com/sites/images/zoho-sites.png\" style=\"padding: 0px 10px 0px 5px;border:0px;\"></a></p></div>"
            script.parentNode.insertBefore(addiv,script);
            __ZSAD.adSpaces.push(addiv.id);
        
        }
        __ZSAD.docScripts.splice(0,len);
    }
    __ZSAD.addFloaterAd = function() {
    }
    __ZSAD.findScripts();
    __ZSAD.processScripts();
}
}());
if(__ZSAD.timmer)clearTimeout(__ZSAD.timmer);
__ZSAD.timmer = setTimeout(function() {
__ZSAD.findScripts();
__ZSAD.processScripts();
},100);
